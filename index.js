const BASE_URL = "https://62db6ca4d1d97b9e0c4f338f.mockapi.io";

function getDSSV() {
    batLoading();
    axios({
        url: `${BASE_URL}/sv`,
        method: "GET",
    })
        .then(function (res) {
            renderDSSV(res.data);
            tatLoading();
        })
        .catch(function (err) {
            tatLoading();
            console.log(err);
        });
}
getDSSV();
function xoaSV(id) {
    axios({
        url: `${BASE_URL}/sv/${id}`,
        method: "DELETE",
    })
        .then(function (res) {
            getDSSV();
            console.log(res);
        })
        .catch(function (err) {
            console.log(err);
        });
}

function themSV() {
    newSV = info();
    axios({
        url: `${BASE_URL}/sv`,
        method: "POST",
        data: newSV,
    })
        .then(function (res) {
            tatLoading();
            console.log(res);
            getDSSV();
        })
        .catch(function (err) {
            tatLoading();
            console.log(err);
        });
}

function editSV(id) {
    axios({
        url: `${BASE_URL}/sv/${id}`,
        method: "GET",
    })
        .then(function (res) {
            console.log(res);
            sua(res.data);
        })
        .catch(function (err) {
            console.log(err);
        });
}
function upSV() {
    id = document.getElementById("txtMaSV").value;
    newSV = info();
    axios({
        url: `${BASE_URL}/sv/${id}`,
        method: "PUT",
        data: newSV,
    })
        .then(function (res) {
            getDSSV();
            console.log(res);
        })
        .catch(function (err) {
            console.log(err);
        });
    resetAll();
}

function rs() {
    resetAll();
}
