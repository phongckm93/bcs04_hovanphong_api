function batLoading() {
    document.getElementById("loading").style.display = "flex";
}
function tatLoading() {
    document.getElementById("loading").style.display = "none";
}
renderDSSV = function (dssv) {
    var contentHTML = "";
    dssv.forEach((sv) => {
        var a = (sv.math * 1 + sv.chemistry * 1 + sv.physics * 1) / 3;
        var aFix = a.toFixed(1);
        var contentTr = ` <tr>
        <td>${sv.id}</td>
        <td>${sv.name}</td>
        <td>${sv.email}</td>
        <td>${aFix}</td>
        <td>
        <button type="button" class="btn btn-danger" onclick=xoaSV("${sv.id}")>Delete</button>
        <a href="#top"><button type="button" class="btn btn-warning" onclick=editSV("${sv.id}")>edit</button></a>
        </td>
        </tr>`;
        contentHTML += contentTr;
        document.getElementById("tbodySinhVien").innerHTML = contentHTML;
    });
};
function info() {
    const tenSV = document.getElementById("txtTenSV").value;
    const email = document.getElementById("txtEmail").value;
    const matKhau = document.getElementById("txtPass").value;
    const diemToan = document.getElementById("txtDiemToan").value;
    const diemLy = document.getElementById("txtDiemLy").value;
    const diemHoa = document.getElementById("txtDiemHoa").value;
    return new SinhVien(tenSV, email, matKhau, diemToan, diemLy, diemHoa);
}

function sua(dssv) {
    document.getElementById("txtMaSV").disabled = true;
    document.getElementById("txtMaSV").value = dssv.id;
    document.getElementById("txtTenSV").value = dssv.name;
    document.getElementById("txtPass").value = dssv.password;
    document.getElementById("txtEmail").value = dssv.email;
    document.getElementById("txtDiemToan").value = dssv.math;
    document.getElementById("txtDiemLy").value = dssv.chemistry;
    document.getElementById("txtDiemHoa").value = dssv.physics;
}
function capNhat(dssv) {
    dssv.name = document.getElementById("txtTenSV").value;
    dssv.email = document.getElementById("txtEmail").value;
    dssv.math = document.getElementById("txtDiemToan").value;
    dssv.chemistry = document.getElementById("txtDiemLy").value;
    dssv.physic = document.getElementById("txtDiemHoa").value;
}

function resetAll() {
    document.getElementById("txtMaSV").value = "";
    document.getElementById("txtTenSV").value = "";
    document.getElementById("txtPass").value = "";
    document.getElementById("txtEmail").value = "";
    document.getElementById("txtDiemToan").value = "";
    document.getElementById("txtDiemLy").value = "";
    document.getElementById("txtDiemHoa").value = "";
    document.getElementById("txtMaSV").disabled = false;
    console.log("ok");
}
